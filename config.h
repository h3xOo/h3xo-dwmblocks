#ifndef CONFIG_H__
#define CONFIG_H__
// Modify this file to change what commands output to your statusbar, and
// recompile using the make command.

static Block const blocks[] = {
        /*Icon*/ /*Command*/ /*Update Interval*/ /*Update Signal*/
    // {"⌨", "sb-kbselect", 0, 30},
        { "", "sb-pacpackages", 0, 8 },
        { "", "sb-memory", 10, 14 },
        { "", "sb-cpu", 10, 18 },
        { "", "sb-volume", 0, 10 },
        { "", "sb-battery", 5, 3 },
        { "", "sb-clock", 30, 1 },
        { "", "sb-internet", 5, 4 },
        { "", "sb-help-icon", 0, 15 },
};

// Sets delimiter between status commands. NULL character ('\0') means no
// delimiter.
static char const *delim = " ";

#endif /* CONFIG_H__ */
