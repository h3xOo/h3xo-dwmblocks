.POSIX:

PREFIX = /usr/local
CC = gcc
LD = gcc

CFLAGS = -O2 -Wall -Wextra

LDFLAGS = -lX11 -fuse-ld=mold

all: dwmblocks

dwmblocks: dwmblocks.o
	$(LD) dwmblocks.o $(LDFLAGS) -o dwmblocks
dwmblocks.o: dwmblocks.c config.h
	$(CC) -c dwmblocks.c $(CFLAGS)
clean:
	rm -f *.o *.gch dwmblocks
install: dwmblocks
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f dwmblocks $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/dwmblocks
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dwmblocks

.PHONY: clean install uninstall all
